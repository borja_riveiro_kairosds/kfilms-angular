it('Visit and load detail page', () => {
  cy.visit('/');
  cy.get(
    '[ng-reflect-router-link="/detail,movie,453395"] > app-card-ui > .card__container > .card__img'
  ).click();
  cy.get('.overview__title').contains('Overview');
  cy.get(
    'app-cast-carrousel-ui > .carrousel__container > .carrousel__header > .carrousel__title'
  ).contains('Cast');
  cy.get(
    'app-carrousel-ui > .carrousel__container > .carrousel__header > .carrousel__title'
  ).contains('Similar');
});
