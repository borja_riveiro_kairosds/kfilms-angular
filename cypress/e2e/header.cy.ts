it('Header print in all pages', () => {
  cy.visit('/');
  cy.get('.nav__list > :nth-child(2) > a').click();
  cy.get('.grid__title').contains('Movies');
  cy.get('.nav__list > :nth-child(1) > a').click();
  cy.get(
    '[title="Trending movies"] > .carrousel__container > .carrousel__header > .carrousel__title'
  ).contains('Trending movies');
  cy.get('.nav__list > :nth-child(3) > a').click();
  cy.get('.grid__title').contains('Series');
  cy.get('h1').click();
  cy.get(
    '[title="Trending movies"] > .carrousel__container > .carrousel__header > .carrousel__title'
  ).contains('Trending movies');
});
