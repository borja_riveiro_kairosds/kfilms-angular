it('Visit catalog', () => {
  cy.visit('/catalog/movie');
  cy.get('.grid__title').contains('Movies');
  cy.visit('/catalog/tv');
  cy.visit('/catalog/movie').contains('Series');
});

it('Use load more button', () => {
  cy.visit('/catalog/movie');
  cy.get('.grid__button').click();
});

it('Search pokemon movies and load more', () => {
  cy.visit('/catalog/movie');
  cy.get('#searchInput').type('Pokemon');
  cy.get('.search__button').click();
  cy.get('.grid__button').click();
});
