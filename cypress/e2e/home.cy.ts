it('All components render in home', () => {
  cy.visit('/');
  cy.get('h1').contains('KFilms');
  cy.get(
    '[title="Trending movies"] > .carrousel__container > .carrousel__header > .carrousel__title'
  ).contains('Trending movies');
  cy.get(
    '[title="Trending series"] > .carrousel__container > .carrousel__header > .carrousel__title'
  ).contains('Trending series');
  cy.get(
    '[title="Top rated movies"] > .carrousel__container > .carrousel__header > .carrousel__title'
  ).contains('Top rated movies');
  cy.get(
    '[title="Top rated series"] > .carrousel__container > .carrousel__header > .carrousel__title'
  ).contains('Top rated series');
});
