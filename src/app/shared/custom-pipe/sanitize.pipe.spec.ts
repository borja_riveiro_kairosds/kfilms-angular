import { TestBed } from '@angular/core/testing';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { SanitizePipe } from './sanitize.pipe';

describe('SanitizePipe', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [BrowserModule],
      providers: [SanitizePipe, DomSanitizer],
    });
  });

  it('create an instance', () => {
    let pipe: SanitizePipe = TestBed.inject(SanitizePipe);
    expect(pipe).toBeTruthy();
  });
});
