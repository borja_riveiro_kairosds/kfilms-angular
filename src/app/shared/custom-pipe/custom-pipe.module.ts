import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PlaceholderImagePipe } from './placeholder-image.pipe';
import { SanitizePipe } from './sanitize.pipe';

@NgModule({
  declarations: [SanitizePipe, PlaceholderImagePipe],
  imports: [CommonModule],
  exports: [SanitizePipe, PlaceholderImagePipe],
})
export class CustomPipeModule {}
