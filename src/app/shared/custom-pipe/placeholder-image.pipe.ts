import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'placeholderImage',
})
export class PlaceholderImagePipe implements PipeTransform {
  transform(value: string): string {
    if (value.includes('null')) {
      return '../../../assets/placeholderW500.png';
    }
    return value;
  }
}
