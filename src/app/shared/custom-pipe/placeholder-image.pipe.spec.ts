import { TestBed } from '@angular/core/testing';
import { PlaceholderImagePipe } from './placeholder-image.pipe';

describe('PlaceholderImagePipe', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlaceholderImagePipe],
    });
  });
  it('create an instance', () => {
    let pipe: PlaceholderImagePipe = TestBed.inject(PlaceholderImagePipe);
    expect(pipe).toBeTruthy();
  });

  it('should tranform when include null', () => {
    let pipe: PlaceholderImagePipe = TestBed.inject(PlaceholderImagePipe);
    expect(pipe.transform('https://image.tmdb.org/t/p/w500/null/')).toBe(
      '../../../assets/placeholderW500.png'
    );
  });
  it('should return same value when no include null', () => {
    let pipe: PlaceholderImagePipe = TestBed.inject(PlaceholderImagePipe);
    expect(pipe.transform('https://image.tmdb.org/t/p/w500/path')).toBe(
      'https://image.tmdb.org/t/p/w500/path'
    );
  });
});
