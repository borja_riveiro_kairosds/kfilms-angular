import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CardUiModule } from '../card-ui/card-ui.module';
import { CarrouselUiComponent } from './carrousel-ui.component';

@NgModule({
  declarations: [CarrouselUiComponent],
  imports: [CommonModule, CardUiModule, RouterModule],
  exports: [CarrouselUiComponent],
})
export class CarrouselUiModule {}
