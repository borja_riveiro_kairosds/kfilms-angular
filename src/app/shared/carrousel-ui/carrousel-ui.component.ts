import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-carrousel-ui',
  templateUrl: './carrousel-ui.component.html',
  styleUrls: ['../../../reset.css', './carrousel-ui.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CarrouselUiComponent implements OnInit {
  @Input() cards: Observable<any> | undefined;
  @Input() category: String | undefined;
  @Input() title: String | undefined;
  @Input() viewMore: Boolean | undefined;

  constructor() {}

  ngOnInit(): void {}
}
