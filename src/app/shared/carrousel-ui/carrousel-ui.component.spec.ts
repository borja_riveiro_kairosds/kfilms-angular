import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarrouselUiComponent } from './carrousel-ui.component';

describe('CarrouselUiComponent', () => {
  let component: CarrouselUiComponent;
  let fixture: ComponentFixture<CarrouselUiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CarrouselUiComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CarrouselUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
