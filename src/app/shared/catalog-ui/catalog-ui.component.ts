import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-catalog-ui',
  templateUrl: './catalog-ui.component.html',
  styleUrls: ['../../../reset.css', './catalog-ui.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CatalogUiComponent implements OnInit {
  @Input() cards: Observable<any> | undefined;
  @Input() category: String | undefined;
  @Input() title: String | undefined;

  @Output() loadMoreEvent = new EventEmitter();
  @Output() search = new EventEmitter();

  public searchForm!: FormGroup;

  constructor() {}

  ngOnInit(): void {
    this.searchForm = new FormGroup({
      searchInput: new FormControl(''),
    });
  }

  loadMore() {
    this.loadMoreEvent.emit();
  }

  handleSearch() {
    this.search.emit(this.searchForm.value.searchInput);
    console.log(this.searchForm.value.searchInput);
  }
}
