import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CardUiModule } from '../card-ui/card-ui.module';
import { CatalogUiComponent } from './catalog-ui.component';

@NgModule({
  declarations: [CatalogUiComponent],
  imports: [CommonModule, CardUiModule, RouterModule, ReactiveFormsModule],
  exports: [CatalogUiComponent],
})
export class CatalogUiModule {}
