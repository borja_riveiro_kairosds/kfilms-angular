import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrailerUiComponent } from './trailer-ui.component';

describe('TrailerUiComponent', () => {
  let component: TrailerUiComponent;
  let fixture: ComponentFixture<TrailerUiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrailerUiComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TrailerUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
