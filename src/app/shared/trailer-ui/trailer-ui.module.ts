import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CustomPipeModule } from '../custom-pipe/custom-pipe.module';
import { TrailerUiComponent } from './trailer-ui.component';

@NgModule({
  declarations: [TrailerUiComponent],
  imports: [CommonModule, CustomPipeModule],
  exports: [TrailerUiComponent],
})
export class TrailerUiModule {}
