import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { SafeResourceUrl } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { TrailerResult } from '../model';

@Component({
  selector: 'app-trailer-ui',
  templateUrl: './trailer-ui.component.html',
  styleUrls: ['./trailer-ui.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrailerUiComponent implements OnInit {
  @Input() trailer: Observable<TrailerResult> | undefined;

  public dangerousVideoUrl: string | undefined;
  public videoUrl: SafeResourceUrl | undefined;

  constructor() {}

  ngOnInit(): void {}
}
