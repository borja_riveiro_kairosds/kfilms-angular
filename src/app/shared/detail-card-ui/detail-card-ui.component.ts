import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-detail-card-ui',
  templateUrl: './detail-card-ui.component.html',
  styleUrls: ['./detail-card-ui.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailCardUiComponent implements OnInit {
  @Input() details: Observable<any> | undefined;

  constructor() {}

  ngOnInit(): void {}
}
