import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CustomPipeModule } from '../custom-pipe/custom-pipe.module';
import { DetailCardUiComponent } from './detail-card-ui.component';

@NgModule({
  declarations: [DetailCardUiComponent],
  imports: [CommonModule, CustomPipeModule],
  exports: [DetailCardUiComponent],
})
export class DetailCardUiModule {}
