import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailCardUiComponent } from './detail-card-ui.component';

describe('DetailCardUiComponent', () => {
  let component: DetailCardUiComponent;
  let fixture: ComponentFixture<DetailCardUiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailCardUiComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DetailCardUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
