import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoaderService {
  public showLoaderObservable$: Observable<boolean>;
  private loaderSubject: Subject<boolean>;

  constructor() {
    this.loaderSubject = new Subject<boolean>();
    this.showLoaderObservable$ = this.loaderSubject.asObservable();
  }

  public show(): void {
    this.loaderSubject.next(true);
  }

  public hide(): void {
    this.loaderSubject.next(false);
  }
}
