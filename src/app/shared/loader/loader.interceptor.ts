import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable } from 'rxjs';
import { LoaderService } from './loader.service';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {
  public activeRequest: number = 0;

  constructor(private loaderService: LoaderService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    if (this.activeRequest === 0) {
      console.log('Interceptor dentro del show');

      this.loaderService.show();
    }
    console.log('Interceptor fuera del show');
    this.activeRequest++;

    return next.handle(request).pipe(
      finalize(() => {
        console.log('Interceptor en el finalize');
        this.activeRequest--;
        if (this.activeRequest === 0) {
          this.loaderService.hide();
          console.log('Interceptor en el finalize con hide');
        }
      })
    );
  }
}
