export interface TrendingMoviesDTO {
  page: number;
  results: TrendingMoviesResultDTO[];
  total_pages: number;
  total_results: number;
}

export interface TrendingSeriesDTO {
  page: number;
  results: TrendingSeriesResultDTO[];
  total_pages: number;
  total_results: number;
}

export interface TopRatedMoviesDTO {
  page: number;
  results: TopRatedMoviesResultDTO[];
  total_pages: number;
  total_results: number;
}

export interface TopRatedSeriesDTO {
  page: number;
  results: TopRatedSeriesResultDTO[];
  total_pages: number;
  total_results: number;
}

export interface TrendingMoviesResultDTO {
  genre_ids: number[];
  original_language: string;
  original_title: string;
  poster_path: string;
  video: boolean;
  vote_average: number;
  id: number;
  overview: string;
  release_date: string;
  vote_count: number;
  title: string;
  adult: boolean;
  backdrop_path: string;
  popularity: number;
  media_type: string;
}

export interface TrendingSeriesResultDTO {
  original_name: string;
  id: number;
  first_air_date: string;
  name: string;
  backdrop_path: string;
  vote_count: number;
  genre_ids: number[];
  vote_average: number;
  original_language: string;
  overview: string;
  poster_path: string;
  origin_country: string[];
  popularity: number;
  media_type: string;
}

export interface TopRatedMoviesResultDTO {
  adult: boolean;
  backdrop_path: string;
  genre_ids: number[];
  id: number;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
}

export interface TopRatedSeriesResultDTO {
  backdrop_path?: string;
  first_air_date: string;
  genre_ids: number[];
  id: number;
  name: string;
  origin_country: string[];
  original_language: string;
  original_name: string;
  overview: string;
  popularity: number;
  poster_path: string;
  vote_average: number;
  vote_count: number;
}

export interface DetailsDTO {
  adult: boolean;
  backdrop_path: string;
  belongs_to_collection: object;
  budget: number;
  genres: any[];
  homepage: string;
  id: number;
  imdb_id: string;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  production_companies: any[];
  production_countries: any[];
  release_date: string;
  revenue: number;
  runtime: number;
  spoken_languages: any[];
  status: string;
  tagline: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
  name?: string;
}

export interface CreditsDTO {
  id: number;
  cast: CreditsCastDTO[];
}

export interface CreditsCastDTO {
  adult: boolean;
  gender: number;
  id: number;
  known_for_department: string;
  name: string;
  original_name: string;
  popularity: number;
  profile_path: string;
  cast_id: number;
  character: string;
  credit_id: string;
  order: number;
}

export interface TrailerDTO {
  id: number;
  results: TrailerResultDTO[];
}

export interface TrailerResultDTO {
  iso_639_1: string;
  iso_3166_1: string;
  name: string;
  key: string;
  site: string;
  size: number;
  type: string;
  official: boolean;
  published_at: string;
  id: string;
}

export interface SimilarDTO {
  page: number;
  results: SimilarResultDTO[];
  total_pages: number;
  total_results: number;
}

export interface SimilarResultDTO {
  adult: boolean;
  backdrop_path: string;
  genre_ids: number[];
  id: number;
  title: string;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
}

export interface CatalogDTO {
  page: number;
  results: CatalogResultDTO[];
  total_pages: number;
  total_results: number;
}

export interface CatalogResultDTO {
  adult: boolean;
  backdrop_path: string;
  genre_ids: number[];
  id: number;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
  name?: string;
}
