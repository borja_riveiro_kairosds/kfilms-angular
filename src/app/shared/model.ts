export interface RootResult {
  id: number;
  title: string;
  poster_path: string;
  name?: string;
}

export interface DetailsResult {
  title?: string;
  name?: string;
  overview: string;
  vote_average: number;
  poster_path: string;
  genres: string[];
}

export interface CastResult {
  profile_path: string;
  name: string;
  original_name: string;
  character: string;
}

export interface TrailerResult {
  name: string;
  key: string;
}
