import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CastCarrouselUiComponent } from './cast-carrousel-ui.component';

describe('CastCarrouselUiComponent', () => {
  let component: CastCarrouselUiComponent;
  let fixture: ComponentFixture<CastCarrouselUiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CastCarrouselUiComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CastCarrouselUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
