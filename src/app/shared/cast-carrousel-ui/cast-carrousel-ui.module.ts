import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardUiModule } from '../card-ui/card-ui.module';
import { CastCarrouselUiComponent } from './cast-carrousel-ui.component';

@NgModule({
  declarations: [CastCarrouselUiComponent],
  imports: [CommonModule, CardUiModule],
  exports: [CastCarrouselUiComponent],
})
export class CastCarrouselUiModule {}
