import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-cast-carrousel-ui',
  templateUrl: './cast-carrousel-ui.component.html',
  styleUrls: ['./cast-carrousel-ui.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CastCarrouselUiComponent implements OnInit {
  @Input() casts: Observable<any> | undefined;

  constructor() {}

  ngOnInit(): void {}
}
