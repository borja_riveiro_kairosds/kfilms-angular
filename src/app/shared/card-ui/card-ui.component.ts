import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';

@Component({
  selector: 'app-card-ui',
  templateUrl: './card-ui.component.html',
  styleUrls: ['../../../reset.css', './card-ui.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardUiComponent implements OnInit {
  @Input() imgPath: String | undefined;
  @Input() title: String | undefined;
  @Input() subtitle: String | undefined;

  constructor() {}

  ngOnInit(): void {}
}
