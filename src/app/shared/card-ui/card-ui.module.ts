import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CustomPipeModule } from '../custom-pipe/custom-pipe.module';
import { CardUiComponent } from './card-ui.component';

@NgModule({
  declarations: [CardUiComponent],
  imports: [CommonModule, CustomPipeModule],
  exports: [CardUiComponent],
})
export class CardUiModule {}
