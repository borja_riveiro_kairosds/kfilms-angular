import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import {
  CreditsDTO,
  DetailsDTO,
  SimilarDTO,
  TrailerDTO,
} from 'src/app/shared/dto';

@Injectable({
  providedIn: 'root',
})
export class DetailProxyService {
  constructor(private httpClient: HttpClient) {}

  private apiUrl: string = 'https://api.themoviedb.org/3';
  private apiKey: string = '0052c2ec6752583df22660a4ef5e9df1';

  getDetail(category: string, id: number): Observable<DetailsDTO> {
    return this.httpClient.get<DetailsDTO>(
      `${this.apiUrl}/${category}/${id}?api_key=${this.apiKey}`
    );
  }

  getCast(category: string, id: number): Observable<CreditsDTO> {
    return this.httpClient.get<CreditsDTO>(
      `${this.apiUrl}/${category}/${id}/credits?api_key=${this.apiKey}`
    );
  }

  getTrailer(category: string, id: number): Observable<TrailerDTO> {
    return this.httpClient.get<TrailerDTO>(
      `${this.apiUrl}/${category}/${id}/videos?api_key=${this.apiKey}`
    );
  }

  getSimilar(category: string, id: number): Observable<SimilarDTO> {
    return this.httpClient.get<SimilarDTO>(
      `${this.apiUrl}/${category}/${id}/similar?api_key=${this.apiKey}`
    );
  }
}
