import { Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import {
  CastResult,
  DetailsResult,
  RootResult,
  TrailerResult,
} from 'src/app/shared/model';
import { DetailService } from '../detail.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css'],
})
export class DetailComponent implements OnInit, OnDestroy {
  public details$: Observable<DetailsResult> | undefined;
  public casts$: Observable<CastResult[]> | undefined;
  public trailer$: Observable<TrailerResult> | undefined;
  public similar$: Observable<RootResult[]> | undefined;
  public category!: string;
  public id!: number;
  private sub: Subscription | undefined;

  constructor(
    private titleService: Title,
    private service: DetailService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.sub = this.activatedRoute.params.subscribe((routeParams) => {
      this.id = routeParams['id'];
      this.category = routeParams['category'];
      this.details$ = this.service.getDetail(this.category, this.id);
      this.casts$ = this.service.getCast(this.category, this.id);
      this.trailer$ = this.service.getTrailer(this.category, this.id);
      this.similar$ = this.service.getSimilar(this.category, this.id);
      this.titleService.setTitle('KFilms - Detail');
    });
  }

  ngOnDestroy() {
    this.sub?.unsubscribe();
  }
}
