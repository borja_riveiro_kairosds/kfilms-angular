import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CarrouselUiModule } from 'src/app/shared/carrousel-ui/carrousel-ui.module';
import { CastCarrouselUiModule } from '../../shared/cast-carrousel-ui/cast-carrousel-ui.module';
import { DetailCardUiModule } from '../../shared/detail-card-ui/detail-card-ui.module';
import { TrailerUiModule } from '../../shared/trailer-ui/trailer-ui.module';
import { DetailRoutingModule } from './detail-routing.module';
import { DetailComponent } from './detail/detail.component';

@NgModule({
  declarations: [DetailComponent],
  imports: [
    CommonModule,
    DetailRoutingModule,
    DetailCardUiModule,
    CastCarrouselUiModule,
    TrailerUiModule,
    CarrouselUiModule,
  ],
})
export class DetailModule {}
