import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import {
  CreditsCastDTO,
  CreditsDTO,
  DetailsDTO,
  SimilarDTO,
  SimilarResultDTO,
  TrailerDTO,
  TrailerResultDTO,
} from 'src/app/shared/dto';
import {
  CastResult,
  DetailsResult,
  RootResult,
  TrailerResult,
} from 'src/app/shared/model';
import { DetailProxyService } from './detail-proxy.service';

@Injectable({
  providedIn: 'root',
})
export class DetailService {
  constructor(private proxyService: DetailProxyService) {}

  getDetail(category: string, id: number) {
    return this.proxyService.getDetail(category, id).pipe(
      map((detailsDTO: DetailsDTO) => {
        const detailsResult: DetailsResult = {
          title: detailsDTO.title,
          overview: detailsDTO.overview,
          vote_average: detailsDTO.vote_average,
          poster_path: detailsDTO.poster_path,
          genres: detailsDTO.genres,
          name: detailsDTO.name,
        };
        return detailsResult;
      })
    );
  }

  getCast(category: string, id: number) {
    return this.proxyService.getCast(category, id).pipe(
      map((creditsDTO: CreditsDTO) => {
        let cast: CastResult[] = [];
        creditsDTO.cast
          .filter((item) => item.known_for_department === 'Acting')
          .slice(0, 20)
          .forEach((creditsCastDTO: CreditsCastDTO) => {
            const castModel: CastResult = {
              profile_path: creditsCastDTO.profile_path,
              name: creditsCastDTO.name,
              original_name: creditsCastDTO.original_name,
              character: creditsCastDTO.character,
            };
            cast = [...cast, castModel];
          });
        return cast;
      })
    );
  }

  getTrailer(category: string, id: number) {
    return this.proxyService.getTrailer(category, id).pipe(
      map((trailerDTO: TrailerDTO) => {
        let trailerResult: TrailerResult = {
          name: '',
          key: '',
        };
        trailerDTO.results
          .filter((video) => video.type === 'Trailer')
          .slice(0, 1)
          .forEach((trailerResultDTO: TrailerResultDTO) => {
            trailerResult = {
              name: trailerResultDTO.name,
              key: trailerResultDTO.key,
            };
          });
        return trailerResult;
      })
    );
  }

  getSimilar(category: string, id: number) {
    return this.proxyService.getSimilar(category, id).pipe(
      map((similarDTO: SimilarDTO) => {
        let similarResult: RootResult[] = [];
        similarDTO.results.forEach((similarResultDTO: SimilarResultDTO) => {
          const similar: RootResult = {
            id: similarResultDTO.id,
            title: similarResultDTO.title,
            poster_path: similarResultDTO.poster_path,
          };
          similarResult = [...similarResult, similar];
        });
        return similarResult;
      })
    );
  }
}
