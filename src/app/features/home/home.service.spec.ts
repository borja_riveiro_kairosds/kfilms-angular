import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { of } from 'rxjs';
import { HomeProxyService } from './home-proxy.service';
import { HomeService } from './home.service';

import { RootResult } from 'src/app/shared/model';
import { topRatedMovies } from '../../../../fixtures/topRatedMovies';
import { topRatedSeries } from '../../../../fixtures/topRatedSeries';
import { trendingMovies } from '../../../../fixtures/trendingMovies';
import { trendingSeries } from '../../../../fixtures/trendingSeries';

describe('HomeService', () => {
  let service: HomeService;
  let proxy: HomeProxyService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(HomeService);
    proxy = TestBed.inject(HomeProxyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should mapper dto to model on trending movies', waitForAsync(() => {
    const spyProxy = jest
      .spyOn(proxy, 'getTrendingMovies')
      .mockReturnValue(of(trendingMovies));
    service.getTrendingMovies().subscribe((data: RootResult[]) => {
      expect(data[0].id).toEqual(trendingMovies.results[0].id);
      expect(data[0].poster_path).toEqual(
        trendingMovies.results[0].poster_path
      );
    });
    expect(spyProxy).toHaveBeenCalled();
  }));

  it('should mapper dto to model on trending series', waitForAsync(() => {
    const spyProxy = jest
      .spyOn(proxy, 'getTrendingSeries')
      .mockReturnValue(of(trendingSeries));
    service.getTrendingSeries().subscribe((data: RootResult[]) => {
      expect(data[0].id).toEqual(trendingSeries.results[0].id);
      expect(data[0].poster_path).toEqual(
        trendingSeries.results[0].poster_path
      );
    });
    expect(spyProxy).toHaveBeenCalled();
  }));

  it('should mapper dto to model on top rated movies', waitForAsync(() => {
    const spyProxy = jest
      .spyOn(proxy, 'getTopRatedMovies')
      .mockReturnValue(of(topRatedMovies));
    service.getTopRatedMovies().subscribe((data: RootResult[]) => {
      expect(data[0].id).toEqual(topRatedMovies.results[0].id);
      expect(data[0].poster_path).toEqual(
        topRatedMovies.results[0].poster_path
      );
    });
    expect(spyProxy).toHaveBeenCalled();
  }));

  it('should mapper dto to model on top rated series', waitForAsync(() => {
    const spyProxy = jest
      .spyOn(proxy, 'getTopRatedSeries')
      .mockReturnValue(of(topRatedSeries));
    service.getTopRatedSeries().subscribe((data: RootResult[]) => {
      expect(data[0].id).toEqual(topRatedSeries.results[0].id);
      expect(data[0].poster_path).toEqual(
        topRatedSeries.results[0].poster_path
      );
    });
    expect(spyProxy).toHaveBeenCalled();
  }));
});
