import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import {
  TopRatedMoviesDTO,
  TopRatedMoviesResultDTO,
  TopRatedSeriesDTO,
  TopRatedSeriesResultDTO,
  TrendingMoviesDTO,
  TrendingMoviesResultDTO,
  TrendingSeriesDTO,
  TrendingSeriesResultDTO,
} from 'src/app/shared/dto';
import { RootResult } from 'src/app/shared/model';
import { HomeProxyService } from './home-proxy.service';

@Injectable({
  providedIn: 'root',
})
export class HomeService {
  constructor(private proxyService: HomeProxyService) {}

  getTrendingMovies() {
    return this.proxyService.getTrendingMovies().pipe(
      map((trendingMoviesDTO: TrendingMoviesDTO) => {
        let trendingMoviesResult: RootResult[] = [];
        trendingMoviesDTO.results.forEach(
          (trendingMoviesResultDTO: TrendingMoviesResultDTO) => {
            const trendingMovieResult: RootResult = {
              poster_path: trendingMoviesResultDTO.poster_path,
              id: trendingMoviesResultDTO.id,
              title: trendingMoviesResultDTO.title,
            };
            trendingMoviesResult = [
              ...trendingMoviesResult,
              trendingMovieResult,
            ];
          }
        );
        return trendingMoviesResult;
      })
    );
  }

  getTrendingSeries() {
    return this.proxyService.getTrendingSeries().pipe(
      map((trendingSeriesDTO: TrendingSeriesDTO) => {
        let trendingSeriesResult: RootResult[] = [];
        trendingSeriesDTO.results.forEach(
          (trendingSeriesResultDTO: TrendingSeriesResultDTO) => {
            const trendingSerieResult: RootResult = {
              poster_path: trendingSeriesResultDTO.poster_path,
              id: trendingSeriesResultDTO.id,
              title: trendingSeriesResultDTO.name,
            };
            trendingSeriesResult = [
              ...trendingSeriesResult,
              trendingSerieResult,
            ];
          }
        );
        return trendingSeriesResult;
      })
    );
  }

  getTopRatedMovies() {
    return this.proxyService.getTopRatedMovies().pipe(
      map((topRatedMoviesDTO: TopRatedMoviesDTO) => {
        let topRatedMoviesResult: RootResult[] = [];
        topRatedMoviesDTO.results.forEach(
          (topRatedMoviesResultDTO: TopRatedMoviesResultDTO) => {
            const topRatedMovieResult: RootResult = {
              poster_path: topRatedMoviesResultDTO.poster_path,
              id: topRatedMoviesResultDTO.id,
              title: topRatedMoviesResultDTO.title,
            };
            topRatedMoviesResult = [
              ...topRatedMoviesResult,
              topRatedMovieResult,
            ];
          }
        );
        return topRatedMoviesResult;
      })
    );
  }

  getTopRatedSeries() {
    return this.proxyService.getTopRatedSeries().pipe(
      map((topRatedSeriesDTO: TopRatedSeriesDTO) => {
        let topRatedSeriesResult: RootResult[] = [];
        topRatedSeriesDTO.results.forEach(
          (topRatedSeriesResultDTO: TopRatedSeriesResultDTO) => {
            const topRatedSerieResult: RootResult = {
              poster_path: topRatedSeriesResultDTO.poster_path,
              id: topRatedSeriesResultDTO.id,
              title: topRatedSeriesResultDTO.name,
            };
            topRatedSeriesResult = [
              ...topRatedSeriesResult,
              topRatedSerieResult,
            ];
          }
        );
        return topRatedSeriesResult;
      })
    );
  }
}
