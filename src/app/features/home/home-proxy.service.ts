import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import {
  TopRatedMoviesDTO,
  TopRatedSeriesDTO,
  TrendingMoviesDTO,
  TrendingSeriesDTO,
} from 'src/app/shared/dto';

@Injectable({
  providedIn: 'root',
})
export class HomeProxyService {
  constructor(private httpClient: HttpClient) {}

  private apiUrl: string = 'https://api.themoviedb.org/3';
  private apiKey: string = '0052c2ec6752583df22660a4ef5e9df1';

  getTrendingMovies(): Observable<TrendingMoviesDTO> {
    return this.httpClient.get<TrendingMoviesDTO>(
      `${this.apiUrl}/trending/movie/day?api_key=${this.apiKey}`
    );
  }

  getTrendingSeries(): Observable<TrendingSeriesDTO> {
    return this.httpClient.get<TrendingSeriesDTO>(
      `${this.apiUrl}/trending/tv/day?api_key=${this.apiKey}`
    );
  }

  getTopRatedMovies(): Observable<TopRatedMoviesDTO> {
    return this.httpClient.get<TopRatedMoviesDTO>(
      `${this.apiUrl}/movie/top_rated?api_key=${this.apiKey}`
    );
  }

  getTopRatedSeries(): Observable<TopRatedSeriesDTO> {
    return this.httpClient.get<TopRatedSeriesDTO>(
      `${this.apiUrl}/tv/top_rated?api_key=${this.apiKey}`
    );
  }
}
