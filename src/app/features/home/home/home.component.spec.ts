import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import '@borjariveiro/three-dot-spinner/three-dot-spinner.js';
import { of } from 'rxjs';
import { RootResult } from 'src/app/shared/model';
import { topRatedMovies } from '../../../../../fixtures/topRatedMovies';
import { topRatedSeries } from '../../../../../fixtures/topRatedSeries';
import { trendingMovies } from '../../../../../fixtures/trendingMovies';
import { trendingSeries } from '../../../../../fixtures/trendingSeries';
import { HomeProxyService } from '../home-proxy.service';
import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let proxy: HomeProxyService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomeComponent],
      imports: [HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    proxy = TestBed.inject(HomeProxyService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get trending movies', waitForAsync(() => {
    const spyProxy = jest
      .spyOn(proxy, 'getTrendingMovies')
      .mockImplementation(() => of(trendingMovies));

    component.ngOnInit();

    component.trendingMovies$.subscribe((data: RootResult[]) => {
      expect(data[0].id).toEqual(trendingMovies.results[0].id);
      expect(data[0].poster_path).toEqual(
        trendingMovies.results[0].poster_path
      );
    });
    expect(spyProxy).toHaveBeenCalled();
    spyProxy.mockRestore();
  }));

  it('should get trending series', waitForAsync(() => {
    const spyProxy = jest
      .spyOn(proxy, 'getTrendingSeries')
      .mockImplementation(() => of(trendingSeries));

    component.ngOnInit();

    component.trendingSeries$.subscribe((data: RootResult[]) => {
      expect(data[0].id).toEqual(trendingSeries.results[0].id);
      expect(data[0].poster_path).toEqual(
        trendingSeries.results[0].poster_path
      );
    });
    expect(spyProxy).toHaveBeenCalled();
    spyProxy.mockRestore();
  }));

  it('should get top rated movies', waitForAsync(() => {
    const spyProxy = jest
      .spyOn(proxy, 'getTopRatedMovies')
      .mockImplementation(() => of(topRatedMovies));

    component.ngOnInit();

    component.topRatedMovies$.subscribe((data: RootResult[]) => {
      expect(data[0].id).toEqual(topRatedMovies.results[0].id);
      expect(data[0].poster_path).toEqual(
        topRatedMovies.results[0].poster_path
      );
    });
    expect(spyProxy).toHaveBeenCalled();
    spyProxy.mockRestore();
  }));

  it('should get top rated series', waitForAsync(() => {
    const spyProxy = jest
      .spyOn(proxy, 'getTopRatedSeries')
      .mockImplementation(() => of(topRatedSeries));

    component.ngOnInit();

    component.topRatedSeries$.subscribe((data: RootResult[]) => {
      expect(data[0].id).toEqual(topRatedSeries.results[0].id);
      expect(data[0].poster_path).toEqual(
        topRatedSeries.results[0].poster_path
      );
    });
    expect(spyProxy).toHaveBeenCalled();
    spyProxy.mockRestore();
  }));
});
