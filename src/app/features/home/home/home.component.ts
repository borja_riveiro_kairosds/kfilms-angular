import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import '@borjariveiro/three-dot-spinner/three-dot-spinner.js';
import { Observable } from 'rxjs/internal/Observable';
import { LoaderService } from 'src/app/shared/loader/loader.service';
import { RootResult } from 'src/app/shared/model';
import { HomeService } from '../home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  public trendingMovies$: Observable<RootResult[]>;
  public trendingSeries$: Observable<RootResult[]>;
  public topRatedMovies$: Observable<RootResult[]>;
  public topRatedSeries$: Observable<RootResult[]>;
  public loaderSpinner$: Observable<boolean>;

  constructor(
    private titleService: Title,
    private service: HomeService,
    private loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.loaderSpinner$ = this.loaderService.showLoaderObservable$;

    this.titleService.setTitle('KFilms - Home');
    this.trendingMovies$ = this.service.getTrendingMovies();
    this.trendingSeries$ = this.service.getTrendingSeries();
    this.topRatedMovies$ = this.service.getTopRatedMovies();
    this.topRatedSeries$ = this.service.getTopRatedSeries();
  }
}
