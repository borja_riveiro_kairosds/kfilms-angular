import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { topRatedMovies } from '../../../../fixtures/topRatedMovies';
import { topRatedSeries } from '../../../../fixtures/topRatedSeries';
import { trendingMovies } from '../../../../fixtures/trendingMovies';
import { trendingSeries } from '../../../../fixtures/trendingSeries';
import { HomeProxyService } from './home-proxy.service';

describe('HomeProxyService', () => {
  let service: HomeProxyService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(HomeProxyService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should verify getTrendingMovies request', () => {
    service.getTrendingMovies().subscribe((movies) => {
      expect(movies).toBe(trendingMovies);
    });
    const request = httpMock.expectOne(
      'https://api.themoviedb.org/3/trending/movie/day?api_key=0052c2ec6752583df22660a4ef5e9df1'
    );
    expect(request.request.method).toEqual('GET');
    request.flush(trendingMovies);
    httpMock.verify();
  });

  it('should verify getTrendingSeries request', () => {
    service.getTrendingSeries().subscribe((movies) => {
      expect(movies).toBe(trendingSeries);
    });
    const request = httpMock.expectOne(
      'https://api.themoviedb.org/3/trending/tv/day?api_key=0052c2ec6752583df22660a4ef5e9df1'
    );
    expect(request.request.method).toEqual('GET');
    request.flush(trendingSeries);
    httpMock.verify();
  });

  it('should verify topRatedMovies request', () => {
    service.getTopRatedMovies().subscribe((movies) => {
      expect(movies).toBe(topRatedMovies);
    });
    const request = httpMock.expectOne(
      'https://api.themoviedb.org/3/movie/top_rated?api_key=0052c2ec6752583df22660a4ef5e9df1'
    );
    expect(request.request.method).toEqual('GET');
    request.flush(topRatedMovies);
    httpMock.verify();
  });

  it('should verify topRatedSeries request', () => {
    service.getTopRatedSeries().subscribe((movies) => {
      expect(movies).toBe(topRatedSeries);
    });
    const request = httpMock.expectOne(
      'https://api.themoviedb.org/3/tv/top_rated?api_key=0052c2ec6752583df22660a4ef5e9df1'
    );
    expect(request.request.method).toEqual('GET');
    request.flush(topRatedSeries);
    httpMock.verify();
  });
});
