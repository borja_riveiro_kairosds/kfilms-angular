import { TestBed } from '@angular/core/testing';

import { CatalogStoreService } from './catalog-store.service';

describe('CatalogStoreService', () => {
  let service: CatalogStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CatalogStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
