import { Injectable } from '@angular/core';
import { firstValueFrom, tap } from 'rxjs';
import { RootResult } from 'src/app/shared/model';
import { Store } from 'src/app/shared/state/data';
import { CatalogService } from './catalog.service';

@Injectable({
  providedIn: 'root',
})
export class CatalogStoreService extends Store<RootResult[]> {
  constructor(private catalogService: CatalogService) {
    super();
  }

  init(category: string, page: number, query: string): Promise<RootResult[]> {
    return firstValueFrom(
      this.catalogService.getCatalog(category, page, query).pipe(
        tap((catalogResult) => {
          this.store(catalogResult);
        })
      )
    );
  }

  loadMore(
    category: string,
    page: number,
    query: string
  ): Promise<RootResult[]> {
    return firstValueFrom(
      this.catalogService.getCatalog(category, page, query).pipe(
        tap((catalogResult) => {
          this.store([...this.get(), ...catalogResult]);
        })
      )
    );
  }
}
