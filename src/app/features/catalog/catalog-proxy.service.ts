import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CatalogDTO } from 'src/app/shared/dto';

@Injectable({
  providedIn: 'root',
})
export class CatalogProxyService {
  constructor(private httpClient: HttpClient) {}

  private apiUrl: string = 'https://api.themoviedb.org/3';
  private apiKey: string = '0052c2ec6752583df22660a4ef5e9df1';

  getCatalog(category: string, page: number, query: string) {
    const urlEndPoint = query ? `/search/${category}` : `/${category}/popular`;
    return this.httpClient.get<CatalogDTO>(
      `${this.apiUrl}${urlEndPoint}?api_key=${this.apiKey}&page=${page}&query=${query}`
    );
  }
}
