import { TestBed } from '@angular/core/testing';

import { CatalogProxyService } from './catalog-proxy.service';

describe('CatalogProxyService', () => {
  let service: CatalogProxyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CatalogProxyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
