import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CatalogUiModule } from '../../shared/catalog-ui/catalog-ui.module';
import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog/catalog.component';

@NgModule({
  declarations: [CatalogComponent],
  imports: [CommonModule, CatalogRoutingModule, CatalogUiModule],
})
export class CatalogModule {}
