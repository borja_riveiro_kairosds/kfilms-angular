import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { CatalogDTO, CatalogResultDTO } from 'src/app/shared/dto';
import { RootResult } from 'src/app/shared/model';
import { CatalogProxyService } from './catalog-proxy.service';

@Injectable({
  providedIn: 'root',
})
export class CatalogService {
  constructor(private proxyService: CatalogProxyService) {}

  getCatalog(category: string, page: number, query: string) {
    return this.proxyService.getCatalog(category, page, query).pipe(
      map((catalogDTO: CatalogDTO) => {
        let catalogResult: RootResult[] = [];
        catalogDTO.results.forEach((catalogResultDTO: CatalogResultDTO) => {
          const catalog: RootResult = {
            id: catalogResultDTO.id,
            title: catalogResultDTO.title,
            poster_path: catalogResultDTO.poster_path,
            name: catalogResultDTO.name,
          };
          catalogResult = [...catalogResult, catalog];
        });
        return catalogResult;
      })
    );
  }
}
