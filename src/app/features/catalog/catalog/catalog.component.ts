import { Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { RootResult } from 'src/app/shared/model';
import { CatalogStoreService } from '../catalog-store.service';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['../../../../reset.css', './catalog.component.css'],
})
export class CatalogComponent implements OnInit, OnDestroy {
  public catalog$!: Observable<RootResult[]>;
  public category!: string;
  public keyword!: string;
  public titleCategory!: string;
  public page: number = 1;
  private sub: Subscription | undefined;

  constructor(
    private catalogStore: CatalogStoreService,
    private titleService: Title,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.sub = this.activatedRoute.params.subscribe((routeParams) => {
      this.page = 1;
      this.category = routeParams['category'];
      this.keyword = routeParams['keyword'];
      if (routeParams['category'] === 'movie') {
        this.titleCategory = 'Movies';
      } else {
        this.titleCategory = 'Series';
      }
      this.titleService.setTitle(`KFilms - ${this.titleCategory}`);

      this.catalogStore.init(this.category, this.page, this.keyword);
      this.catalog$ = this.catalogStore.get$();
    });
  }

  loadMore() {
    this.page++;
    this.catalogStore.loadMore(this.category, this.page, this.keyword);
  }

  handleSearch(event: string) {
    this.router.navigate(['catalog', this.category, 'search', event]);
  }

  ngOnDestroy() {
    this.sub?.unsubscribe();
  }
}
