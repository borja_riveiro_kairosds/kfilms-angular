export const trendingMovies = {
  page: 1,
  results: [
    {
      genre_ids: [12, 35, 10749],
      original_language: 'en',
      original_title: 'The Lost City',
      poster_path: '/neMZH82Stu91d3iqvLdNQfqPPyl.jpg',
      video: false,
      vote_average: 6.9,
      id: 752623,
      overview:
        'Follows a reclusive romance novelist who was sure nothing could be worse than getting stuck on a book tour with her cover model, until a kidnapping attempt sweeps them both into a cutthroat jungle adventure, proving life can be so much stranger, and more romantic, than any of her paperback fictions.',
      release_date: '2022-03-24',
      vote_count: 292,
      title: 'The Lost City',
      adult: false,
      backdrop_path: '/A3bsT0m1um6tvcmlIGxBwx9eAxn.jpg',
      popularity: 505.424,
      media_type: 'movie',
    },
    {
      original_title: 'Doctor Strange in the Multiverse of Madness',
      poster_path: '/wRnbWt44nKjsFPrqSmwYki5vZtF.jpg',
      video: false,
      vote_average: 7.5,
      overview:
        'Doctor Strange, with the help of mystical allies both old and new, traverses the mind-bending and dangerous alternate realities of the Multiverse to confront a mysterious new adversary.',
      release_date: '2022-05-04',
      title: 'Doctor Strange in the Multiverse of Madness',
      id: 453395,
      adult: false,
      backdrop_path: '/AdyJH8kDm8xT8IKTlgpEC15ny4u.jpg',
      genre_ids: [14, 28, 12],
      vote_count: 1139,
      original_language: 'en',
      popularity: 5007.839,
      media_type: 'movie',
    },
    {
      original_language: 'en',
      original_title: 'Fantastic Beasts: The Secrets of Dumbledore',
      id: 338953,
      video: false,
      vote_average: 6.8,
      overview:
        "Professor Albus Dumbledore knows the powerful, dark wizard Gellert Grindelwald is moving to seize control of the wizarding world. Unable to stop him alone, he entrusts magizoologist Newt Scamander to lead an intrepid team of wizards and witches. They soon encounter an array of old and new beasts as they clash with Grindelwald's growing legion of followers.",
      release_date: '2022-04-06',
      title: 'Fantastic Beasts: The Secrets of Dumbledore',
      vote_count: 920,
      adult: false,
      backdrop_path: '/wdjdHBDwmynWUrJcNzS24uegvK5.jpg',
      poster_path: '/jrgifaYeUtTnaH7NF5Drkgjg2MB.jpg',
      genre_ids: [14, 12],
      popularity: 826.325,
      media_type: 'movie',
    },
    {
      id: 675353,
      vote_average: 7.7,
      overview:
        'After settling in Green Hills, Sonic is eager to prove he has what it takes to be a true hero. His test comes when Dr. Robotnik returns, this time with a new partner, Knuckles, in search for an emerald that has the power to destroy civilizations. Sonic teams up with his own sidekick, Tails, and together they embark on a globe-trotting journey to find the emerald before it falls into the wrong hands.',
      release_date: '2022-03-30',
      vote_count: 1142,
      adult: false,
      backdrop_path: '/egoyMDLqCxzjnSrWOz50uLlJWmD.jpg',
      video: false,
      genre_ids: [28, 878, 35, 10751, 12],
      title: 'Sonic the Hedgehog 2',
      original_language: 'en',
      original_title: 'Sonic the Hedgehog 2',
      poster_path: '/6DrHO1jr3qVrViUO6s6kFiAGM7.jpg',
      popularity: 12851.144,
      media_type: 'movie',
    },
    {
      backdrop_path: '/aEGiJJP91HsKVTEPy1HhmN0wRLm.jpg',
      title: 'Uncharted',
      genre_ids: [28, 12],
      original_language: 'en',
      original_title: 'Uncharted',
      poster_path: '/tlZpSxYuBRoVJBOpUrPdQe9FmFq.jpg',
      video: false,
      vote_average: 7.2,
      overview:
        'A young street-smart, Nathan Drake and his wisecracking partner Victor “Sully” Sullivan embark on a dangerous pursuit of “the greatest treasure never found” while also tracking clues that may lead to Nathan’s long-lost brother.',
      release_date: '2022-02-10',
      vote_count: 1796,
      id: 335787,
      adult: false,
      popularity: 4449.169,
      media_type: 'movie',
    },
    {
      original_title: 'The Batman',
      id: 414906,
      video: false,
      vote_average: 7.8,
      overview:
        'In his second year of fighting crime, Batman uncovers corruption in Gotham City that connects to his own family while facing a serial killer known as the Riddler.',
      release_date: '2022-03-01',
      title: 'The Batman',
      adult: false,
      backdrop_path: '/5P8SmMzSNYikXpxil6BYzJ16611.jpg',
      vote_count: 4401,
      genre_ids: [80, 9648, 53],
      poster_path: '/74xTEgt7R36Fpooo50r9T25onhq.jpg',
      original_language: 'en',
      popularity: 4740.532,
      media_type: 'movie',
    },
    {
      original_title: 'Loin du périph',
      poster_path: '/h5hVeCfYSb8gIO0F41gqidtb0AI.jpg',
      video: false,
      vote_average: 5.7,
      overview:
        'Ousmane Diakité and François Monge are two cops with very different styles, backgrounds and careers. The unlikely pair are reunited once again for a new investigation that takes them across France. What seemed to be a simple drug deal turns out to be a much bigger criminal case wrapped in danger and unexpected comedy.',
      release_date: '2022-05-06',
      title: 'The Takedown',
      id: 785985,
      adult: false,
      backdrop_path: '/wtbRUVxQVvU6QIJH1oGLDThJLib.jpg',
      genre_ids: [35, 28],
      vote_count: 68,
      original_language: 'fr',
      popularity: 182.687,
      media_type: 'movie',
    },
    {
      adult: false,
      backdrop_path: '/iacx4nWW1HSQC8hfOtIYL4ayi5O.jpg',
      genre_ids: [16, 28, 878],
      id: 965244,
      original_language: 'ja',
      original_title: '攻殻機動隊 SAC_2045 持続可能戦争',
      overview:
        'In the year 2045, after an economic disaster known as the Synchronized Global Default, rapid developments in AI propelled the world to enter a state of "Sustainable War". However, the public is not aware of the threat that AI has towards the human race.  A compilation film with newly added footage.',
      poster_path: '/p5b1c4BKXexVstOBzrBRAWMBj3V.jpg',
      release_date: '2021-12-21',
      title: 'Ghost in the Shell: SAC_2045 Sustainable War',
      video: false,
      vote_average: 8.2,
      vote_count: 5,
      popularity: 2.769,
      media_type: 'movie',
    },
    {
      poster_path: '/1g0dhYtq4irTY1GPXvft6k4YLjm.jpg',
      video: false,
      id: 634649,
      overview:
        'Peter Parker is unmasked and no longer able to separate his normal life from the high-stakes of being a super-hero. When he asks for help from Doctor Strange the stakes become even more dangerous, forcing him to discover what it truly means to be Spider-Man.',
      release_date: '2021-12-15',
      vote_count: 12602,
      adult: false,
      backdrop_path: '/iQFcwSGbZXMkeyKrxbPnwnRo5fl.jpg',
      vote_average: 8.1,
      genre_ids: [28, 12, 878],
      title: 'Spider-Man: No Way Home',
      original_language: 'en',
      original_title: 'Spider-Man: No Way Home',
      popularity: 3933.964,
      media_type: 'movie',
    },
    {
      overview:
        'When the infamous Bad Guys are finally caught after years of countless heists and being the world’s most-wanted villains, Mr. Wolf brokers a deal to save them all from prison.',
      release_date: '2022-03-17',
      adult: false,
      backdrop_path: '/fEe5fe82qHzjO4yej0o79etqsWV.jpg',
      genre_ids: [16, 35, 28, 10751, 80],
      title: 'The Bad Guys',
      original_language: 'en',
      original_title: 'The Bad Guys',
      poster_path: '/7qop80YfuO0BwJa1uXk1DXUUEwv.jpg',
      vote_count: 305,
      video: false,
      id: 629542,
      vote_average: 7.7,
      popularity: 4730.107,
      media_type: 'movie',
    },
    {
      poster_path: '/fjaoD0ZfPOf2C5BalCziPUaf4Zk.jpg',
      video: false,
      id: 76600,
      overview:
        'Set more than a decade after the events of the first film, “Avatar: The Way of Water” begins to tell the story of the Sully family (Jake, Neytiri, and their kids), the trouble that follows them, the lengths they go to keep each other safe, the battles they fight to stay alive,  and the tragedies they endure.',
      release_date: '2022-12-14',
      vote_count: 0,
      adult: false,
      backdrop_path: '/fkGR1ltNbvERk3topo4dP3gWsvR.jpg',
      vote_average: 0.0,
      genre_ids: [28, 12, 14, 878],
      title: 'Avatar: The Way of Water',
      original_language: 'en',
      original_title: 'Avatar: The Way of Water',
      popularity: 267.336,
      media_type: 'movie',
    },
    {
      release_date: '2022-04-27',
      id: 829557,
      backdrop_path: '/t9hNIUFcy0tYu55IPxjxRTTdZ6X.jpg',
      genre_ids: [10749, 18],
      vote_count: 346,
      original_language: 'pl',
      original_title: '365 Days: This Day',
      poster_path: '/7qU0SOVcQ8BTJLodcAlulUAG16C.jpg',
      title: '365 Days: This Day',
      video: false,
      vote_average: 5.7,
      adult: false,
      overview:
        "Laura and Massimo are back and hotter than ever. But the reunited couple's new beginning is complicated by Massimo’s family ties and a mysterious man who enters Laura’s life to win her heart and trust, at any cost.",
      popularity: 1823.374,
      media_type: 'movie',
    },
    {
      adult: false,
      backdrop_path: '/xqlxMhXkLGHHDiFwVYzYTayUFyA.jpg',
      genre_ids: [10749, 18],
      id: 778106,
      original_language: 'en',
      original_title: 'Along for the Ride',
      overview:
        'The summer before college Auden meets the mysterious Eli, a fellow insomniac. While the seaside town of Colby sleeps, the two embark on a nightly quest to help Auden experience the fun, carefree teen life she never knew she wanted.',
      poster_path: '/d5spmLeGR9kxBRQ6qxCFad1ljvT.jpg',
      release_date: '2022-05-06',
      title: 'Along for the Ride',
      video: false,
      vote_average: 6.8,
      vote_count: 46,
      popularity: 164.174,
      media_type: 'movie',
    },
    {
      genre_ids: [10752, 36, 18],
      original_language: 'en',
      id: 661231,
      poster_path: '/hHdtWTSFFYSdoInT3xqxCviIARx.jpg',
      video: false,
      vote_average: 6.5,
      overview:
        'In 1943, two British intelligence officers concoct Operation Mincemeat, wherein their plan to drop a corpse with false papers off the coast of Spain would fool Nazi spies into believing the Allied forces were planning to attack by way of Greece rather than Sicily.',
      release_date: '2022-04-01',
      vote_count: 58,
      title: 'Operation Mincemeat',
      adult: false,
      backdrop_path: '/xaqAINN4n0LtvqQ2MSKrVkFpYgV.jpg',
      original_title: 'Operation Mincemeat',
      popularity: 30.827,
      media_type: 'movie',
    },
    {
      original_language: 'en',
      original_title: 'Marmaduke',
      poster_path: '/tvhX4QQnMyMjlFeShLCXovvbf0c.jpg',
      video: false,
      vote_average: 5.4,
      overview:
        'Great Dane Marmaduke epitomizes the overgrown lapdog, with an irascible streak and a penchant for mischief that is tempered with a deep sense of love and responsibility for his human family, the Winslows. The new animation is set in the world of elite dog shows, rife divas, rivalries and slapstick comedy.',
      id: 678287,
      vote_count: 5,
      title: 'Marmaduke',
      adult: false,
      backdrop_path: '/f8F4XgaIXidVLqmJGYFP9jHH08Y.jpg',
      release_date: '2022-04-28',
      genre_ids: [16, 10751, 35],
      popularity: 71.021,
      media_type: 'movie',
    },
    {
      overview:
        "Decorated veteran Will Sharp, desperate for money to cover his wife's medical bills, asks for help from his adoptive brother Danny. A charismatic career criminal, Danny instead offers him a score: the biggest bank heist in Los Angeles history: $32 million.",
      release_date: '2022-03-16',
      adult: false,
      backdrop_path: '/192vHmAPbk5esL34XMKZ1YLGFjr.jpg',
      genre_ids: [28, 53, 80],
      title: 'Ambulance',
      original_language: 'en',
      original_title: 'Ambulance',
      poster_path: '/kuxjMVuc3VTD7p42TZpJNsSrM1V.jpg',
      vote_count: 515,
      video: false,
      id: 763285,
      vote_average: 7.0,
      popularity: 2670.696,
      media_type: 'movie',
    },
    {
      backdrop_path: '/fOy2Jurz9k6RnJnMUMRDAgBwru2.jpg',
      title: 'Turning Red',
      genre_ids: [16, 10751, 35, 14],
      original_language: 'en',
      original_title: 'Turning Red',
      poster_path: '/qsdjk9oAKSQMWs0Vt5Pyfh6O4GZ.jpg',
      video: false,
      vote_average: 7.4,
      overview:
        'Thirteen-year-old Mei is experiencing the awkwardness of being a teenager with a twist – when she gets too excited, she transforms into a giant red panda.',
      release_date: '2022-03-10',
      vote_count: 2058,
      id: 508947,
      adult: false,
      popularity: 2493.062,
      media_type: 'movie',
    },
    {
      original_language: 'en',
      original_title: 'Doctor Strange',
      id: 284052,
      video: false,
      vote_average: 7.4,
      overview:
        'After his career is destroyed, a brilliant but arrogant surgeon gets a new lease on life when a sorcerer takes him under her wing and trains him to defend the world against evil.',
      release_date: '2016-10-25',
      title: 'Doctor Strange',
      vote_count: 18894,
      adult: false,
      backdrop_path: '/eQN31P4IEhyp6NkdccvppJnyuJ4.jpg',
      poster_path: '/uGBVj3bEbCoZbDjjl9wTxcygko1.jpg',
      genre_ids: [28, 12, 14, 878],
      popularity: 2807.025,
      media_type: 'movie',
    },
    {
      adult: false,
      backdrop_path: '/x747ZvF0CcYYTTpPRCoUrxA2cYy.jpg',
      genre_ids: [878, 12, 28],
      id: 406759,
      original_language: 'en',
      original_title: 'Moonfall',
      poster_path: '/odVv1sqVs0KxBXiA8bhIBlPgalx.jpg',
      video: false,
      vote_average: 6.5,
      vote_count: 935,
      overview:
        'A mysterious force knocks the moon from its orbit around Earth and sends it hurtling on a collision course with life as we know it.',
      release_date: '2022-02-03',
      title: 'Moonfall',
      popularity: 2231.435,
      media_type: 'movie',
    },
    {
      original_title: 'Top Gun: Maverick',
      poster_path: '/wxP2Mzv9CdjOK6t4dNnFGqIQl0V.jpg',
      title: 'Top Gun: Maverick',
      id: 361743,
      vote_average: 0.0,
      overview:
        "After more than thirty years of service as one of the Navy's top aviators, Pete Mitchell is where he belongs, pushing the envelope as a courageous test pilot and dodging the advancement in rank that would ground him.",
      release_date: '2022-05-24',
      adult: false,
      backdrop_path: '/odJ4hx6g6vBt4lBWKFD1tI8WS4x.jpg',
      vote_count: 0,
      genre_ids: [28, 18],
      video: false,
      original_language: 'en',
      popularity: 161.743,
      media_type: 'movie',
    },
  ],
  total_pages: 1000,
  total_results: 20000,
};
